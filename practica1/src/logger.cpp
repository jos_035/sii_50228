#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <ctype.h>
#include <fcntl.h>
#include <stdlib.h>
#include <unistd.h>
 
int main(int argc, char* argv[]) {

	if(mkfifo("/tmp/loggerfifo",0777)==-1)
	{
		perror("Error en la creacion del FIFO\n");
		exit(1);
	}

 	int fd=open("/tmp/loggerfifo", O_RDONLY);
	if(fd==-1) {
		perror("Error al abrir fichero:");
       		exit(1); 
	}
 
	int salir=0; 
 	int ret;
 	while(salir==0)
 	{
 		char buff[200];
 		ret=read(fd,buff,sizeof(buff));		

		if(ret==-1)
		{
			perror("\n Error en el read");
			exit(1);
		}

 		printf("%s\n", buff);
 		if((buff[0]=='-')||(ret==-1))
 		{
			printf("----Tenis cerrado. Cerrando logger...--- \n");
 			salir=1; 
 		}
 	}

 	close(fd);
	
	bool a=unlink("/tmp/loggerfifo");

	if(a==0)
	{
		perror("Error al borrar el fifo");
		exit(1);	
	}

	return 0;
 }

